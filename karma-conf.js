// Karma configuration
// Generated on Thu Jul 09 2015 10:13:58 GMT+0200 (CEST)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
      'client/bower_components/jquery/dist/jquery.js',
      'client/bower_components/angular/angular.js',
      'client/bower_components/ng-lodash/build/ng-lodash.js',
      'client/bower_components/allmighty-autocomplete/script/autocomplete.js',
      'client/bower_components/angular-facebook/lib/angular-facebook.js',
      'client/bower_components/angular-resource/angular-resource.js',
      'client/bower_components/angular-route/angular-route.js',
      'client/bower_components/angular-ui-router/release/angular-ui-router.js',
      'client/bower_components/angular-sanitize/angular-sanitize.js',
      'client/bower_components/angular-mocks/angular-mocks.js',
      'client/bower_components/ng-file-upload/ng-file-upload.js',
      'client/bower_components/angular-messages/angular-messages.js',
      'client/bower_components/angular-utils-pagination/dirPagination.js',
      'client/app.js',
      'client/recipes/recipes.js',
      'client/app.routes.js',
      'client/components/recipe.factory.js',
      'client/components/ingredient.factory.js',
      'client/components/current_user.service.js',
      'client/components/search_ingredients.directive.js',
      'client/app.controller.js',
      'client/recipes/recipes.controller.js',
      'client/recipes/recipe/recipe.controller.js',
      'client/recipes/new_recipe/new_recipe.controller.js',
      'client/recipes/edit_recipe/edit_recipe.controller.js',
      'client/recipes/recipe/destroy_recipe/destroy_recipe.controller.js',
      'client/**/*.spec.js'
    ],


    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false
  })
}
