class AddAbbreviationToUnits < ActiveRecord::Migration
  def change
    add_column :units, :abbreviation, :string
  end
end
