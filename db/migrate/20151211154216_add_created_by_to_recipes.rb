class AddCreatedByToRecipes < ActiveRecord::Migration
  def change
    add_column :recipes, :created_by, :string, null: false
  end
end
