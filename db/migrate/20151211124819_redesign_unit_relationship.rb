class RedesignUnitRelationship < ActiveRecord::Migration
  def change
    remove_index :units, column: :name, unique: true

    drop_table :units do |t|
      t.string :name, null: false
      t.string :abbreviation
      t.timestamps null: false
    end

    remove_column :recipes_ingredients, :unit_id, :integer, null: false
    change_column :recipes_ingredients, :amount, :string, null: false
  end
end
