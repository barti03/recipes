class CreateRecipesIngredients < ActiveRecord::Migration
  def change
    create_table :recipes_ingredients do |t|
      t.integer :amount, null: false
      t.references :unit, null: false
      t.references :ingredient, null: false
      t.references :recipe, null: false

      t.timestamps null: false
    end

    add_index :recipes_ingredients, [:ingredient_id, :recipe_id], unique: true
    add_index :recipes_ingredients, :unit_id
  end
end
