r1 = Recipe.create(name: "Pizza Cones", directions: "<p>Sed eget mattis dolor, varius sagittis massa. Quisque sed lacus sed felis condimentum congue. Maecenas ligula nulla, convallis sit amet nisl sit amet, venenatis pharetra risus. Donec turpis justo, rutrum in ipsum sit amet, facilisis finibus justo. Ut scelerisque ornare purus in porttitor. Etiam et justo nulla. Quisque commodo nulla eu ligula elementum pulvinar. Fusce a bibendum turpis, eu pretium risus. Suspendisse luctus vel mi at aliquet. Integer vel leo non urna auctor bibendum nec a ante. Cras quis facilisis ante. Quisque auctor justo nibh, ut scelerisque est vulputate sit amet. Morbi placerat nunc fringilla vehicula porttitor. Mauris luctus tristique dolor at malesuada.</p><p>Vestibulum auctor erat vel imperdiet aliquam. Cras quis urna eros. Integer ligula nunc, maximus quis convallis in, laoreet vitae lectus. Praesent cursus lorem sed urna porttitor lobortis. Sed ut gravida eros. Vivamus hendrerit placerat nisl. Etiam tortor quam, finibus vitae urna quis, dictum porttitor justo. Phasellus sollicitudin tellus nec tortor bibendum eleifend.</p>")

r2 = Recipe.create(name: "Best Hamburger Recipe", directions:  "<p>Aliquam fermentum leo orci, in consequat sem gravida sit amet. Maecenas elementum placerat nisi faucibus sodales. In tempus nunc a nisl auctor, dictum porta massa ullamcorper. Morbi molestie orci vitae dolor vulputate euismod non eu tortor. Nunc ut semper lacus, ut facilisis neque. Sed consectetur sodales mi, at faucibus sapien varius non. Praesent semper cursus metus vitae commodo. Cras orci ex, ultricies ultricies maximus sed, faucibus eu quam. Nulla ullamcorper mi velit, ut pretium nisi laoreet a. Nunc vitae quam lorem. Aliquam erat volutpat. Aliquam ultrices aliquet dui eu hendrerit. Donec vitae nunc ac nibh hendrerit blandit quis quis enim. Ut arcu velit, gravida id sapien ut, porta aliquet augue. Aenean euismod vehicula tellus, non hendrerit lacus eleifend a. Aenean mauris diam, ornare non maximus et, interdum vitae nulla.</p><p>Vestibulum aliquet mi nec justo blandit tempor. Etiam tincidunt, magna in eleifend eleifend, odio ligula egestas nisl, eget pharetra elit nulla eget metus. Phasellus varius, nibh vel cursus placerat, urna justo lobortis turpis, sed elementum ante velit in risus. Duis a dapibus ex. Proin porttitor molestie mi, et iaculis sapien sodales ut. Fusce quis elit ut leo sagittis tempor. Fusce ut viverra sapien. Mauris sagittis ultrices maximus. Etiam ultricies erat sit amet finibus aliquam.</p>")

r3 = Recipe.create(name: "Chicken Nuggets", directions:  "<p>Fusce a scelerisque augue. Cras luctus at lectus quis porttitor. Cras dolor lacus, luctus id erat sit amet, scelerisque pellentesque quam. Suspendisse sagittis fermentum tellus, in tincidunt ligula tempus vitae. Nam quis tempor est. Phasellus mi odio, sodales eu libero nec, rutrum dignissim urna. Donec pretium ex ac malesuada tincidunt. Sed facilisis ornare tempus. Fusce a pellentesque neque. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam fermentum odio nunc, sed tincidunt metus facilisis vel. Vestibulum enim mi, tincidunt id suscipit eget, tincidunt quis mi. Aenean nec eleifend nibh. Donec a venenatis sem. Donec a finibus elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit</p><p>Aenean finibus lectus ut neque mollis, quis aliquam augue condimentum. Nulla libero elit, mattis ut commodo quis, ultrices eu arcu. Cras egestas nunc nec diam semper pellentesque. Sed auctor gravida efficitur. Nulla in libero in nisi elementum finibus. Ut quis massa tempor, ultrices mi sit amet, posuere libero. Donec aliquet sodales augue, non lacinia est posuere eu. Sed id maximus tellus, ac blandit purus. Vivamus ac justo est. Proin ultricies tellus scelerisque urna mollis posuere. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque porttitor condimentum enim. Morbi maximus sagittis condimentum. Ut bibendum nisi et lorem viverra blandit. Proin in libero nec leo elementum varius vel et augue.</p>")

i1 = Ingredient.create(name: "Flour")
i2 = Ingredient.create(name: "Poork")
i3 = Ingredient.create(name: "Salat")
i4 = Ingredient.create(name: "Bread")
i5 = Ingredient.create(name: "Milk")
i6 = Ingredient.create(name: "Chicken")

u1 = Unit.create(name: "mililitre", abbreviation: "ml")
u2 = Unit.create(name: "gramme", abbreviation: "g")
u3 = Unit.create(name: "spoon")
u4 = Unit.create(name: "glass")

r1.recipes_ingredients.create(ingredient: i1, unit: u4, amount: 2)
r1.recipes_ingredients.create(ingredient: i2, unit: u2, amount: 200)
r1.recipes_ingredients.create(ingredient: i5, unit: u1, amount: 150)

r1.recipes_ingredients.create(ingredient: i2, unit: u2, amount: 250)
r1.recipes_ingredients.create(ingredient: i4, unit: u2, amount: 300)

r1.recipes_ingredients.create(ingredient: i6, unit: u2, amount: 200)
r1.recipes_ingredients.create(ingredient: i2, unit: u2, amount: 200)
r1.recipes_ingredients.create(ingredient: i5, unit: u1, amount: 150)
