// gulP
var gulp = require('gulp');

// plugins
var connect = require('gulp-connect'),
    sass = require('gulp-sass'),
    url = require('url'),
    proxy = require('proxy-middleware'),
    jshint = require('gulp-jshint'),
    del = require('del'),
    uglify = require('gulp-uglify');
 
// tasks
gulp.task('lint', function(){
  return gulp.src(['./client/**/*.js', '!./client/bower_components/**'])
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(jshint.reporter('fail'));
});

gulp.task('del', function(){
  return del(['./public/*', '!./public/system/**'], {force: true});
});

gulp.task('sass', function(){
  return gulp.src('./client/application.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./client/'));
});

gulp.task('sass:watch', function(){
  return gulp.watch('./client/**/*.scss', ['sass'])
});

gulp.task('sass:minify', function(){
  return gulp.src('./client/application.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest('./public/'));
});

gulp.task('js:minify', function(){
  return gulp.src([
    './client/**/*.js',
    '!./client/**/*.spec.js',
    '!./client/bower_components/**'
  ])
    .pipe(uglify({
      mangle: false
    }))
    .pipe(gulp.dest('./public/'));
});

gulp.task('html:copy', function(){
  return gulp.src(['./client/**/*.html', '!./client/bower_components/**'])
    .pipe(gulp.dest('./public/'));
});

gulp.task('bower:copy', function(){
  return gulp.src('./client/bower_components/**')
    .pipe(gulp.dest('./public/bower_components/'));
});

gulp.task('images:copy', function(){
  return gulp.src('./client/images/**')
    .pipe(gulp.dest('./public/images/'));
});

gulp.task('fonts:copy', function(){
  return gulp.src('./client/fonts/**')
    .pipe(gulp.dest('./public/fonts/'));
});

gulp.task('connect', function(){
  return connect.server({
    root: 'client',
    middleware: function(){
      var apiOptions = url.parse('http://localhost:3000/api');
      apiOptions.route = '/api';
      var imgOptions = url.parse('http://localhost:3000/system');
      imgOptions.route = '/system';
      return [
        proxy(apiOptions),
        proxy(imgOptions)
      ];
    }
  });
});

gulp.task('build', ['del'], function(){
  gulp.start(['sass:minify', 'js:minify', 'html:copy', 'bower:copy', 'fonts:copy', 'images:copy'])
}); 

gulp.task('default', ['sass:watch', 'connect']);
