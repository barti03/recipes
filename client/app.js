angular.module(
  'app',
  [
    'ui.router',
    'ngResource',
    'ngSanitize',
    'ngLodash',
    'recipes',
    'facebook',
    'ngFileUpload',
    'ngMessages',
    'angularUtils.directives.dirPagination'
  ]
)
  .config(function(FacebookProvider){
    FacebookProvider.init({
      appId: '1608757146058434',
      version: 'v2.4'
    });
  });
