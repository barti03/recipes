describe('Recipe', function(){
  var Recipe, httpBackend, lodash, Upload, uploader;

  Upload = {};
  uploader = {}
  uploader.then = function(success){
    return success();
  };
  Upload.upload = function(){
    return uploader;
  };

  beforeEach(module('app'));
  beforeEach(module(function($provide){
    $provide.value('Upload', Upload);
  }));
  beforeEach(inject(function(_Recipe_, $httpBackend){
    Recipe = _Recipe_;
    httpBackend = $httpBackend;
  }));

  it('is defined', function(){
    expect(!!Recipe).toBe(true);
  });

  it('should be Resource', function(){
    expect(Recipe.name).toBe("Resource");
  });

  describe('search method', function(){
    it('is defined', function(){
      expect(typeof Recipe.search).toBe('function');
    });

    it('is binded to /api/recipes/search', function(){
      httpBackend.expectGET('/api/recipes/search').respond([]);
      Recipe.search();
      httpBackend.flush();
    });
  });

  describe('where method', function(){
    it('is defined', function(){
      expect(typeof Recipe.where).toBe('function');
    });

    describe('when name is truthy', function(){
      describe('when ingredients are empty', function(){
        it('calls Recipe search method', function(){
          spyOn(Recipe, 'search');
          Recipe.where('pizza', [], function(){}, function(){});
          expect(Recipe.search).toHaveBeenCalled();
        });
      });

      describe('when ingredients are not empty', function(){
        it('calls Recipe search method', function(){
          spyOn(Recipe, 'search');
          Recipe.where(
            'pizza',
            [{name: 'flour', ingredient_id: 55}],
            function(){},
            function(){}
          );
          expect(Recipe.search).toHaveBeenCalled();
        });
      });
    });

    describe('when name is falsey', function(){
      describe('when ingredients are empty', function(){
        it('calls error', function(){
          var error = jasmine.createSpy('error');
          Recipe.where('', [], function(){}, error);
          expect(error).toHaveBeenCalled();
        });
      });

      describe('when ingredients are not empty', function(){
        it('calls Recipe search method', function(){
          spyOn(Recipe, 'search');
          Recipe.where(
            '',
            [{name: 'flour', ingredient_id: 55}],
            function(){},
            function(){}
          );
          expect(Recipe.search).toHaveBeenCalled();
        });
      });
    });
  });

  describe('instance', function(){
    var recipeInstance;
    beforeEach(function(){
      recipeInstance = new Recipe();
    });

    describe('save method', function(){
      it('uploads data', function(){
        spyOn(Upload, 'upload').and.callThrough();
        recipeInstance.save([], 'file', function(){});
        expect(Upload.upload).toHaveBeenCalledWith({
          url: '/api/recipes',
          fields: recipeInstance,
          file: 'file',
          sendFieldsAs: 'form',
          fileFormDataName: 'cover'
        });
      });

      it('calls success function after upload', function(){
        var success = function(){};
        spyOn(uploader, 'then');
        recipeInstance.save([], 'file', success);
        expect(uploader.then).toHaveBeenCalledWith(success);
      });

      it('sets recipes_ingredients_attributes to array of objects containing ingredient_id, amount', function(){
        recipeInstance.recipes_ingredients_attributes = [];
        recipeInstance.save([
          {ingredient_id: 1, amount: "3 tea spoons"},
          {ingredient_id: 2, amount: "10ml"},
          {ingredient_id: 3, amount: "4 cups", name: "labamba"}
        ], 'file', function(){});
        expect(recipeInstance.recipes_ingredients_attributes).toEqual([
          {ingredient_id: 1, amount: "3 tea spoons"},
          {ingredient_id: 2, amount: "10ml"},
          {ingredient_id: 3, amount: "4 cups"}
        ]);
      });
    });

    describe('update method', function(){
      it('uploads data', function(){
        spyOn(Upload, 'upload').and.callThrough();  
        recipeInstance.update(12, [], 'file', function(){});
        expect(Upload.upload).toHaveBeenCalledWith({
          url: '/api/recipes/12',
          method: 'PUT',
          fields: recipeInstance,
          file: 'file',
          sendFieldsAs: 'form',
          fileFormDataName: 'cover'
        });
      });

      it('calls success function after upload', function(){
        var success = function(){};
        spyOn(uploader, 'then');
        recipeInstance.update(12, [], 'file', success);
        expect(uploader.then).toHaveBeenCalledWith(success);
      });

      it('sets recipes_ingredients_attributes to array of objects containing id, ingredient_id, amount', function(){
        recipeInstance.recipes_ingredients_attributes = [];
        recipeInstance.update(12, [
          {id: 2, ingredient_id: 1, amount: "3 tea spoons"},
          {id: 3, ingredient_id: 2, amount: "10ml"},
          {id: 4, ingredient_id: 3, amount: "4 cups", name: "labamba"}
        ], 'file', function(){});
        expect(recipeInstance.recipes_ingredients_attributes).toEqual([
          {id: 2, ingredient_id: 1, amount: "3 tea spoons"},
          {id: 3, ingredient_id: 2, amount: "10ml"},
          {id: 4, ingredient_id: 3, amount: "4 cups"}
        ]);
      });
    });

    describe('destroy method', function(){
      it('send request', function(){
        recipeInstance.id = 12;
        httpBackend.expectDELETE('/api/recipes/'+recipeInstance.id).respond({});
        recipeInstance.destroy(function(){});
        httpBackend.flush();
      });

      it('calls passed function on success', function(){
        var callback = jasmine.createSpy('success');
        recipeInstance.id = 12;
        httpBackend.whenDELETE('/api/recipes/'+recipeInstance.id).respond({});
        recipeInstance.destroy(callback);
        httpBackend.flush();

        expect(callback).toHaveBeenCalled();
      });
    });
  });
});
