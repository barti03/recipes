angular.module('app').factory('Recipe', function($resource, lodash, Upload, $http){
  var Recipe = $resource(
    '/api/recipes/:id',
    {},
    {
      search: {
        method: 'GET',
        isArray: true,
        url: '/api/recipes/search'
      }
    }
  );
  Recipe.where = function(name, ingredients, success, error){
    var searchedQuery = name || null;
    if(lodash.isEmpty(ingredients)){
      var searchedIngredients = null;
    }
    else {
      var searchedIngredients = lodash.map(
        ingredients,
        function(ingredient){ return ingredient.ingredient_id }
      ).join(',')
    }
    if(searchedQuery || searchedIngredients){
      Recipe.search({
        query: searchedQuery,
        ingredients: searchedIngredients 
      }, success);
    }
    else {
      error();
    }
  }

  Recipe.prototype.save = function(ingredients, cover, success){
    this.recipes_ingredients_attributes = lodash.map(ingredients, function(ingredient){
      return {
        ingredient_id: ingredient.ingredient_id,
        amount: ingredient.amount
      };
    });

    Upload.upload({
      url: '/api/recipes',
      fields: this,
      file: cover,
      sendFieldsAs: 'form',
      fileFormDataName: 'cover'
    }).then(success);
  };

  Recipe.prototype.update = function(id, ingredients, cover, success){
    this.recipes_ingredients_attributes = lodash.map(ingredients, function(ingredient){
      return {
        id: ingredient.id,
        ingredient_id: ingredient.ingredient_id,
        amount: ingredient.amount
      };
    });

    Upload.upload({
      url: '/api/recipes/' + id,
      method: 'PUT',
      fields: this,
      file: cover,
      sendFieldsAs: 'form',
      fileFormDataName: 'cover'
    }).then(success);
  };

  Recipe.prototype.destroy = function(success){
    $http({
      method: 'DELETE',
      url: '/api/recipes/'+this.id,
      params: {
        access_token: this.access_token
      }
    }).then(success);
  };

  return Recipe;
});
