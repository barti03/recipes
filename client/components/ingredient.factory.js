angular.module('app').factory('Ingredient', function($resource){
  return $resource(
    '/api/ingredients/:id',
    {},
    {
      search: {
        method: 'GET',
        isArray: true,
        url: '/api/ingredients/search'
      }
    }
  );
});
