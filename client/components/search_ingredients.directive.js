angular.module('app').directive('searchIngredients', function(lodash, Ingredient, $document, $timeout){
  var suggested = [];
  var setSuggestedNames = function(){
    return lodash.map(
      suggested,
      function(model){
        return model.name
      }
    );
  };

  return {
    restrict: 'E',
    replace: true,
    template: "<div class='field'><input ng-model='searchName' ng-change='fetchSuggested(searchName)' ng-focus='fetchSuggested(searchName)' class='field__input' placeholder='Search for ingredients'><ul class='suggestions' ng-show='suggestedNames.length'><li class='suggestions__item' ng-repeat='name in suggestedNames' ng-click='choseElement(name)'>{{name}}</li></ul></div>",
    scope: {
      searchName: "=",
      collection: "="
    },
    link: function(scope, element, attrs){
      var clearSuggestedNames = function(event){
        if(element.find(event.target).length <= 0){
          $timeout(function(){
            scope.suggestedNames = [];
          });
        }
      }

      scope.suggested = suggested;

      scope.fetchSuggested = function(name){
        Ingredient.search({query: name}, function(data){
          suggested = data;
          scope.suggestedNames = setSuggestedNames();
        });
      };

      scope.choseElement = function(suggestion){
        var element = lodash.find(
          suggested,
          function(element){
            return element.name === suggestion
          }
        );
        scope.suggestedNames = [];
        scope.searchName = '';
        scope.collection.push(element);
      };

      $document.on('click', clearSuggestedNames);
      element.on('$destroy', function() {
        $document.off("click", clearSuggestedNames);
      });
    }
  }
});
