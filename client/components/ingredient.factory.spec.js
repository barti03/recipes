describe('Ingredient', function(){
  var Ingredient, httpBackend, lodash;

  beforeEach(module('app'));
  beforeEach(inject(function(_Ingredient_, $httpBackend){
    Ingredient = _Ingredient_;
    httpBackend = $httpBackend;
  }));

  it('is defined', function(){
    expect(!!Ingredient).toBe(true);
  });

  it('should be Resource', function(){
    expect(Ingredient.name).toBe("Resource");
  });

  describe('search method', function(){
    it('is defined', function(){
      expect(typeof Ingredient.search).toBe('function');
    });

    it('is binded to /api/ingredients/search', function(){
      httpBackend.expectGET('/api/ingredients/search').respond([]);
      Ingredient.search();
      httpBackend.flush();
    });
  });
});
