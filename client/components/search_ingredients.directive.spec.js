describe('directive: searchIngredients', function(){
  var element, scope, isolatedScope, Ingredient, timeout;
  var selected = [];
  
  Ingredient = {};
  Ingredient.search = function(params, success){
    var data = [{id: 4, name: "Cheese"}, {id: 5, name: "Blue Cheese"}];
    success(data);
  };

  beforeEach(function(){
    module('app');
    module(function($provide){
      $provide.value('Ingredient', Ingredient);
    });
  });

  beforeEach(inject(function($rootScope, $compile, $timeout){
    scope = $rootScope.$new();
    timeout = $timeout;

    element = "<search-ingredients collection='selected' search-name='searchName'/>"
    
    scope.selected = selected;
    scope.searchName = 'search phrase';

    element = $compile(element)(scope);
    scope.$digest();
    isolatedScope = element.isolateScope();
  }));

  describe('isolatedScope', function(){
    it('sets searchName', function(){
      expect(scope.searchName).toBe(isolatedScope.searchName);
    });

    it('sets collection', function(){
      expect(scope.selected).toBe(isolatedScope.collection);
    });
  });

  describe('fetchSuggested function', function(){
    it('calls searchAction', function(){
      spyOn(Ingredient, 'search');
      isolatedScope.fetchSuggested('Cheese');
      expect(Ingredient.search).toHaveBeenCalled();
    });

    it('sets suggestedNames to array of objects passed to success names', function(){
      isolatedScope.suggestedNames = [];
      isolatedScope.fetchSuggested('Cheese');
      expect(isolatedScope.suggestedNames).toEqual(['Cheese', 'Blue Cheese'])
    });
  });

  describe('choseElement', function(){
    it('sets searchName to empty string', function(){
      isolatedScope.searchName = 'Potato';
      isolatedScope.choseElement({});
      expect(isolatedScope.searchName).toEqual('');
    });

    it('sets suggestedNames to empty string', function(){
      isolatedScope.suggestedNames = ['Potato', 'Pork'];
      isolatedScope.choseElement({});
      expect(isolatedScope.suggestedNames).toEqual([]);
    });

    it('adds item to suggested when name matches', function(){
      matchingElement = {id: 8, name: 'Potato'};
      isolatedScope.suggested = [{id: 4, name: 'Cheese'}, matchingElement];
      isolatedScope.choseElement("Potato");
      expect(isolatedScope.suggested).toContain(matchingElement);
    });
  });

  describe('when clicked outside element', function(){
    it('suggestedNames array is cleared', function(){
      isolatedScope.suggestedNames = ['Pineapple', 'Apple'];
      angular.element('body').trigger($.Event('click'));
      timeout.flush();
      expect(isolatedScope.suggestedNames).toEqual([]);
    });
  });
});
