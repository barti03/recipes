angular.module('app').service('currentUser', function(Facebook, lodash){
  var currentUser = this;
  currentUser.instance = {};

  this.fetch = function(){
    Facebook.getLoginStatus(function(response) {
      if(response.status === 'connected') {
        currentUser.instance.accessToken = response.authResponse.accessToken;
        Facebook.api(
          '/me?fields=first_name',
          function(response){
            currentUser.instance.id = response.id;
            currentUser.instance.first_name = response.first_name;
          }
        );
      }
    });
  }

  this.canEdit = function(object){
    return object.created_by == currentUser.instance.id;
  };

  this.login = function(){
    Facebook.login(function(){
      currentUser.fetch();
    });
  };

  this.logout = function(){
    Facebook.logout(function(){
      currentUser.instance = null;
    });
  };
});
