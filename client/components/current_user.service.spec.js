describe('currentUser', function(){
  var currentUser, Facebook;
  Facebook = function(){};
  Facebook.login = function(success){
    success();
  };
  Facebook.logout = function(success){
    success();
  };

  beforeEach(module('app'));
  beforeEach(module(function($provide){
    $provide.value('Facebook', Facebook);
  }));
  beforeEach(inject(function(_currentUser_){
    currentUser = _currentUser_;
  }));

  it('is defined', function(){
    expect(!!currentUser).toBe(true);
  });

  describe('fetch method', function(){
    beforeEach(function(){
      Facebook.getLoginStatus = function(success){
        var response = {
          status: 'connected',
          authResponse: {
            accessToken: '1234'
          }
        };
        success(response);
      };
    });

    it('calls Facebook getLoginStatus', function(){
      spyOn(Facebook, 'getLoginStatus');
      currentUser.fetch()
      expect(Facebook.getLoginStatus).toHaveBeenCalled();
    });

    describe('if user is connected', function(){
      beforeEach(function(){
        Facebook.api = function(url, success){
          var currentUserInstance = {
            id: '4211',
            first_name: 'Steve',
          };
          success(currentUserInstance);
        };
      });

      it('sets instance access token based on response', function(){
        currentUser.instance.accessToken = null;
        currentUser.fetch();
        expect(currentUser.instance.accessToken).toBe('1234');
      });

      it('calls Facebook api', function(){
        spyOn(Facebook, 'api');
        currentUser.fetch();
        expect(Facebook.api).toHaveBeenCalledWith('/me?fields=first_name', jasmine.any(Function));
      });

      it('sets instance id and first_name based on response from Facebook api', function(){
        currentUser.instance.id = null;
        currentUser.instance.first_name = null;
        currentUser.fetch();
        expect(currentUser.instance.id).toBe('4211')
        expect(currentUser.instance.first_name).toBe('Steve')
      }); 
    });

    describe('if user is not connected', function(){
      beforeEach(function(){
        Facebook.getLoginStatus = function(success){
          var response = {
            status: 'disconnected'
          };
          success(response);
        };
      });

      it('leaves instance blank', function(){
        expect(currentUser.instance).toEqual({});
      });
    });
  });

  describe('canEdit method', function(){
    beforeEach(function(){
      currentUser.instance.id = '1055274';
    });

   describe('when argument has created_by property equal to client instance id', function(){
      it('returns true', function(){
        expect(currentUser.canEdit({created_by: currentUser.instance.id})).toEqual(true);
      });
    });

   describe('when argument created_by property is not equal to client instance id', function(){
      it('returns true', function(){
        expect(currentUser.canEdit({created_by: '2590325'})).toEqual(false);
      });
    });
  });

  describe('login method', function(){
    it('calls Facebook login', function(){
      spyOn(Facebook, 'login');
      currentUser.login()
      expect(Facebook.login).toHaveBeenCalled();
    });
    
    it('calls fetch method', function(){
      spyOn(currentUser, 'fetch');
      currentUser.login();
      expect(currentUser.fetch).toHaveBeenCalled();
    });
  });

  describe('logout method', function(){
    it('calls Facebook logout', function(){
      spyOn(Facebook, 'logout');
      currentUser.logout();
      expect(Facebook.logout).toHaveBeenCalled();
    });
    
    it('sets instance of currentUser to null', function(){
      currentUser.instance = {};
      currentUser.logout();
      expect(currentUser.instance).toBe(null);
    });
  });
});
