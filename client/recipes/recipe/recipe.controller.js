angular.module('recipes').controller('recipeController', function(Recipe, $stateParams){
  var recipeCtrl = this;
  Recipe.get({id: $stateParams.recipeId}, function(data){
    recipeCtrl.recipe = data;
  });
});
