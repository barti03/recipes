describe('recipeController', function(){
  var recipeCtrl,
    response = {
      id: 1,
      name: "Pizza Cones",
      directions: "Mix butter, milk and flour, then...",
      recipes_ingredients: [
        {
          amount: "2 spoons",
          ingredient:
            {
            id: 1,
            name: "Flour"
          }
        },
        {
          amount: "200g",
          ingredient: {
            id: 2,
            name: "Poork"
          }
        }
      ]
    },
    mockRecipe = function(options){};

  mockRecipe.get = function(params, success){
    success(response);
  };

  beforeEach(module('recipes'));
  beforeEach(inject(function($controller){
    recipeCtrl = $controller('recipeController', {
      Recipe: mockRecipe,
      $stateParams: {recipeId: 5}
    });
  }));

  it('is defined', function(){
    expect(!!recipeCtrl).toBeTruthy();
  });

  it('sets recipes to data from the response', function(){
    expect(recipeCtrl.recipe).toBe(response);
  });
});
