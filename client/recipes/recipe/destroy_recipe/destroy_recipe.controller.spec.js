describe('destroyRecipeController', function(){
  var destroyRecipeCtrl, Recipe, currentUser, params, $state;

  Recipe = function(){};
  Recipe.prototype.destroy = function(success){
    success();
  };

  currentUser = {
    instance: {
      id: 4
    }
  };

  params = {
    recipeId: 5
  };

  $state = {
    go: function(){}
  };

  beforeEach(module('recipes'));
  beforeEach(inject(function($controller){
    destroyRecipeCtrl = $controller('destroyRecipeController', {
      $stateParams: params,
      Recipe: Recipe,
      currentUser: currentUser,
      $state: $state
    });
  }));

  it('has recipe property', function(){
    var expectedRecipe = new Recipe();
    expectedRecipe.id = params.recipeId;

    expect(destroyRecipeCtrl.recipe instanceof Recipe).toBeTruthy();
    expect(destroyRecipeCtrl.recipe).toEqual(expectedRecipe);
  });

  describe('destroy method', function(){
    it('sets recipe\'s access_token to user\'s accesToken', function(){
      destroyRecipeCtrl.recipe.access_token = null;
      destroyRecipeCtrl.destroyRecipe()
      expect(destroyRecipeCtrl.recipe.access_token).toEqual(currentUser.instance.accessToken);
    });

    it('calls recipe destroy with proper arguments', function(){
      spyOn(destroyRecipeCtrl.recipe, 'destroy')
      destroyRecipeCtrl.destroyRecipe();
      expect(destroyRecipeCtrl.recipe.destroy).toHaveBeenCalledWith(jasmine.any(Function));
    });

    describe('when ends with success', function(){
      it('redirects to recipes state', function(){
        spyOn($state, 'go');
        destroyRecipeCtrl.destroyRecipe();
        expect($state.go).toHaveBeenCalledWith('app.recipes');
      });
    });
  });
});
