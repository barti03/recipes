angular.module('recipes').controller('destroyRecipeController', function($stateParams, Recipe, currentUser, $state){
  var destroyRecipeCtrl = this;

  destroyRecipeCtrl.recipe = new Recipe()
  destroyRecipeCtrl.recipe.id = $stateParams.recipeId;

  destroyRecipeCtrl.destroyRecipe = function(){
    destroyRecipeCtrl.recipe.access_token = currentUser.instance.accessToken;
    destroyRecipeCtrl.recipe.destroy(function(){
      $state.go('app.recipes');
    });
  };
});

