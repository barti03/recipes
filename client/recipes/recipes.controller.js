angular.module('recipes').controller('recipesController', function(Recipe, $scope, $timeout){
  var recipesCtrl = this;
  recipesCtrl.searchName = '';
  recipesCtrl.selectedIngredients = [];

  recipesCtrl.getAll = function(){
    Recipe.query(function(data){
      recipesCtrl.recipes = data;
    });
  };

  recipesCtrl.search = function(searchedName, ingredientsChosen){
    Recipe.where(
      searchedName,
      ingredientsChosen, 
      function(data){
        recipesCtrl.recipes = data;
      },
      recipesCtrl.getAll
    )
  };

  recipesCtrl.clearSearchParams = function(){
    recipesCtrl.searchName = '';
    recipesCtrl.selectedIngredients = [];
  };

  recipesCtrl.clearSearchName = function(){
    recipesCtrl.searchName = '';
  };

  $scope.$watch('recipesCtrl.selectedIngredients.length', function(){
    $timeout(function(){
      recipesCtrl.search(recipesCtrl.searchName, recipesCtrl.selectedIngredients);
    });
  });

  recipesCtrl.getAll();
});
