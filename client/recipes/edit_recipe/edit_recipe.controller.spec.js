describe('editRecipeController', function(){
  var editRecipeCtrl, Recipe, $stateParams, currentUser, $scope, $state, expectedRecipe, expectedIngredients, data;

  data = {
    id: 5,
    name: 'potatoes',
    directions: 'Mix flour and water',
    cover_wide: 'file',
    recipes_ingredients: [
      {id: 1, amount: '4 pieces', ingredient: {id: 2, name: 'potato'}},
      {id: 2, amount: '2 spoons', ingredient: {id: 3, name: 'milk'}}
    ]
  }

  Recipe = function(){};
  Recipe.get = function(params, success){
    success(data);
  };


  currentUser = function(){};
  currentUser.instance = {};

  $state = {
    go: function(){}
  };

  expectedRecipe = new Recipe();
  expectedRecipe.id = 5;
  expectedRecipe.name = data.name;
  expectedRecipe.directions = data.directions;

  expectedIngredients = [
    {id: 1, ingredient_id: 2, name: 'potato', amount: '4 pieces'},
    {id: 2, ingredient_id: 3, name: 'milk', amount: '2 spoons'}
  ];

  beforeEach(module('recipes'));
  beforeEach(inject(function($controller, $rootScope, _$timeout_, _lodash_){
    $timeout = _$timeout_;

    editRecipeCtrl = $controller('editRecipeController', {
      Recipe: Recipe,
      $stateParams: {recipeId: data.id},
      currentUser: currentUser,
      $state: $state,
      $timeout: $timeout,
      lodash: _lodash_
    });
  }));

  describe('during initialization', function(){
    it('is instance of Recipe', function(){
      expect(editRecipeCtrl.recipe instanceof Recipe).toBeTruthy();
    });

    describe('recipe', function(){
      it('is set based on data from backend', function(){
        expect(editRecipeCtrl.recipe).toEqual(expectedRecipe);
      });
    });

    describe('ingredients', function(){
      it('is set based on data from backend', function(){
        expect(editRecipeCtrl.ingredients).toEqual(expectedIngredients);
      });
    });

    describe('currentCover', function(){
      it('is set based on data from backend', function(){
        expect(editRecipeCtrl.currentCover).toEqual(data.cover_wide);
      });
    });
  });

  describe('saveRecipe method', function(){
    beforeEach(function(){
      editRecipeCtrl.recipe.update = function(){};
    });

    it('sets recipe access_token to currentUser instance accessToken', function(){
      currentUser.instance.accessToken = '1234';
      editRecipeCtrl.recipe.access_token = null;
      editRecipeCtrl.saveRecipe();
      expect(editRecipeCtrl.recipe.access_token).toBe('1234');
    });

    it('calls recipe.save with proper arguments', function(){
      spyOn(editRecipeCtrl.recipe, 'update')
      editRecipeCtrl.saveRecipe();
      expect(editRecipeCtrl.recipe.update).toHaveBeenCalledWith(data.id, expectedIngredients, editRecipeCtrl.cover, jasmine.any(Function));
    });

    it('redirects to app.recipes.recipe with proper id after successful save', function(){
      var response = {
        data: {
          id: 8
        }
      };

      editRecipeCtrl.recipe = {
        update: function(id, ingredients, cover, success){
          success(response);
        },
      };

      spyOn($state, 'go');
      editRecipeCtrl.saveRecipe()
      expect($state.go).toHaveBeenCalledWith('app.recipes.recipe', {recipeId: response.data.id});
    });
  });

  describe('dropIngredient method', function(){
    it('removes from ingredients passed element', function(){
      var element = {id: 5, name: "Cham"};
      editRecipeCtrl.ingredients = [{id: 2, name: 'Cacao'}, element];
      editRecipeCtrl.dropIngredient(element);
      expect(editRecipeCtrl.ingredients).not.toContain(element);
    });
  });
});
