angular.module('recipes').controller('editRecipeController', function(Recipe, $stateParams, lodash, currentUser, $state){
  var editRecipeCtrl = this;

  editRecipeCtrl.recipe = new Recipe();
  editRecipeCtrl.ingredients = [];

  editRecipeCtrl.saveRecipe = function(){
    editRecipeCtrl.recipe.access_token = currentUser.instance.accessToken;
    editRecipeCtrl.recipe.update(editRecipeCtrl.recipe.id, editRecipeCtrl.ingredients, editRecipeCtrl.cover, function(response){
      $state.go('app.recipes.recipe', {recipeId: response.data.id});
    });
  };

  Recipe.get({id: $stateParams.recipeId}, function(data){
    editRecipeCtrl.recipe.id = $stateParams.recipeId;
    editRecipeCtrl.recipe.name = data.name;
    editRecipeCtrl.recipe.directions = data.directions;
    editRecipeCtrl.currentCover = data.cover_wide;
    editRecipeCtrl.ingredients = lodash.map(data.recipes_ingredients, function(ingredient){
      return {
        id: ingredient.id,
        ingredient_id: ingredient.ingredient.id,
        name: ingredient.ingredient.name,
        amount: ingredient.amount
      }
    });
  });

  editRecipeCtrl.dropIngredient = function(ingredient){
    lodash.remove(editRecipeCtrl.ingredients, ingredient);
  };
});
