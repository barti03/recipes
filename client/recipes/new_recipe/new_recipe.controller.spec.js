describe('newRecipeController', function(){
  var newRecipeCtrl, Recipe, currentUser, $scope, $state;

  Recipe = function(){};
  currentUser = function(){};
  currentUser.instance = {};

  beforeEach(module('recipes'));
  beforeEach(inject(function($controller, $rootScope, _$timeout_, _lodash_, _$state_){
    $scope = $rootScope.$new();
    $timeout = _$timeout_;
    $state = _$state_;

    newRecipeCtrl = $controller('newRecipeController', {
      Recipe: Recipe,
      currentUser: currentUser,
      $scope: $scope,
      $timeout: $timeout,
      lodash: _lodash_
    });
  }));

  describe('recipe', function(){
    it('is instance of Recipe', function(){
      expect(newRecipeCtrl.recipe instanceof Recipe).toBeTruthy();
    });
  });

  describe('ingredients', function(){
    it('is empty array', function(){
      expect(newRecipeCtrl.ingredients).toEqual([]);
    });
  });

  describe('saveRecipe', function(){
    beforeEach(function(){
      newRecipeCtrl.recipe.save = function(){};
    });

    it('sets recipe access_token to currentUser instance accessToken', function(){
      currentUser.instance.accessToken = '1234';
      newRecipeCtrl.recipe.access_token = null;
      newRecipeCtrl.saveRecipe();
      expect(newRecipeCtrl.recipe.access_token).toBe('1234');
    });

    it('calls recipe.save with proper arguments', function(){
      newRecipeCtrl.ingredients = [{id: 2, name: 'cacao'}];
      newRecipeCtrl.cover = 'file';
      spyOn(newRecipeCtrl.recipe, 'save')
      newRecipeCtrl.saveRecipe();
      expect(newRecipeCtrl.recipe.save).toHaveBeenCalledWith(newRecipeCtrl.ingredients, newRecipeCtrl.cover, jasmine.any(Function));
    });

    it('redirects to app.recipes.recipe with proper id after successful save', function(){
      newRecipeCtrl.recipe = {
        save: function(ingredients, cover, success){
          var response = {
            data: {
              id: 8
            }
          };
          success(response);
        }
      };

      newRecipeCtrl.ingredients = [{id: 2, name: 'cacao'}];
      newRecipeCtrl.cover = 'file';
      spyOn($state, 'go');
      newRecipeCtrl.saveRecipe()
      expect($state.go).toHaveBeenCalledWith('app.recipes.recipe', {recipeId: 8});
    });
  });

  describe('dropIngredient method', function(){
    it('removes from ingredients passed element', function(){
      var element = {id: 5, name: "Cham"};
      newRecipeCtrl.ingredients = [{id: 2, name: 'Cacao'}, element];
      newRecipeCtrl.dropIngredient(element);
      expect(newRecipeCtrl.ingredients).not.toContain(element);
    });
  });
});
