angular.module('recipes').controller('newRecipeController', function(Recipe, currentUser, $scope, lodash, $state){
  var newRecipeCtrl = this;

  newRecipeCtrl.recipe = new Recipe();
  newRecipeCtrl.ingredients = [];

  newRecipeCtrl.dropIngredient = function(ingredient){
    lodash.remove(newRecipeCtrl.ingredients, ingredient);
  };

  newRecipeCtrl.saveRecipe = function(){
    newRecipeCtrl.recipe.access_token = currentUser.instance.accessToken;
    newRecipeCtrl.recipe.save(newRecipeCtrl.ingredients, newRecipeCtrl.cover, function(resp){
      $state.go('app.recipes.recipe', {recipeId: resp.data.id});
    });
  };
});
