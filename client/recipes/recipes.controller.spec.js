describe('recipesController', function(){
  var recipesCtrl,
    $scope,
    $timeout,
    queryResponse = [
      {id: 1, name: 'Burger'},
      {id: 2, name: 'Fresh salad'},
      {id: 3, name: 'French frites'}
    ],
    Recipe = function(){};

  Recipe.query = function(success){
    success(queryResponse);
  };

  Recipe.where = function(searchedName, ingredientsChosen, success, error){
  };

  beforeEach(module('recipes'));
  beforeEach(inject(function($controller, $rootScope, _$timeout_){
    $scope = $rootScope.$new();
    $timeout = _$timeout_

    recipesCtrl = $controller('recipesController', {
      Recipe: Recipe,
      $scope: $scope,
      $timeout: _$timeout_
    });
  }));

  it('is defined', function(){
    expect(!!recipesCtrl).toBeTruthy();
  });

  it('sets recipes to data from the query response', function(){
    expect(recipesCtrl.recipes).toBe(queryResponse);
  });

  describe('method search', function(){
    it('calls Recipe.where', function(){
      spyOn(Recipe, 'where');
      recipesCtrl.search('pizza', []);
      expect(Recipe.where).toHaveBeenCalled();
    });
  });

  describe('method getAll', function(){
    it('sets recipes to data from the query response', function(){
      recipesCtrl.recipes = [];
      recipesCtrl.getAll();
      expect(recipesCtrl.recipes).toBe(queryResponse);
    });
  });

  describe('method clearSearchParams', function(){
    it('clears values of searchName and selectedIngredients', function(){
      recipesCtrl.searchName = 'Bana';
      recipesCtrl.selectedIngredients = [{id: '7', name: 'Banan'}];
      recipesCtrl.clearSearchParams();
      expect(recipesCtrl.searchName).toEqual('');
      expect(recipesCtrl.selectedIngredients).toEqual([]);
    });
  });

  describe('method clearSearchName', function(){
    it('clears value of searchName', function(){
      recipesCtrl.searchName = 'Bana';
      recipesCtrl.clearSearchName();
      expect(recipesCtrl.searchName).toEqual('');
    });
  });

  describe('on selectedIngredients.length change', function(){
    it('calls invokes search function', function(){
      recipesCtrl.searchName = 'Pizza';
      recipesCtrl.selectedIngredients = [];
      spyOn(recipesCtrl, 'search');
      recipesCtrl.selectedIngredients.push({id: 4, name: 'salami'});
      $scope.$digest();
      $timeout.flush();
      expect(recipesCtrl.search).toHaveBeenCalledWith(recipesCtrl.searchName, recipesCtrl.selectedIngredients);
    });
  });
});
