angular.module('app').config(function($stateProvider, $urlRouterProvider){
  $urlRouterProvider.otherwise('/recipes');

  $stateProvider
    .state('app', {
      url: '',
      abstract: true,
      controller: 'appController as appCtrl',
      template: '<ui-view/>'
    })
    .state('app.recipes', {
      url: '/recipes',
      controller: 'recipesController as recipesCtrl',
      templateUrl: 'recipes/recipes.tmpl.html'
    })
    .state('app.recipes.new_recipe', {
      url: '/new',
      controller: 'newRecipeController as formCtrl',
      templateUrl: 'recipes/new_recipe/new_recipe.tmpl.html'
    })
    .state('app.recipes.edit_recipe', {
      url: '/:recipeId/edit',
      controller: 'editRecipeController as formCtrl',
      templateUrl: 'recipes/edit_recipe/edit_recipe.tmpl.html'
    })
    .state('app.recipes.recipe', {
      url: '/:recipeId',
      controller: 'recipeController as recipeCtrl',
      templateUrl: 'recipes/recipe/recipe.tmpl.html'
    })
    .state('app.recipes.recipe.destroy_recipe', {
      url: '/destroy',
      controller: 'destroyRecipeController as destroyRecipeCtrl',
      templateUrl: 'recipes/recipe/destroy_recipe/destroy_recipe.tmpl.html'
    })
});
