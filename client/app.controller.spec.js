describe('appController', function(){
  var appCtrl, currentUser, Facebook;

  currentUser = function(){};
  currentUser.fetch = function(){};

  beforeEach(module('app'));
  beforeEach(inject(function($controller){
    spyOn(currentUser, 'fetch');
    appCtrl = $controller('appController', {
      currentUser: currentUser
    });
  }));

  it('invokes currentUser fetch method', function(){
    expect(currentUser.fetch).toHaveBeenCalled();
  });

  describe('property currentUser', function(){
    it('is set to currentUser service', function(){
      expect(appCtrl.currentUser).toBe(currentUser);
    });
  });
});
