angular.module('app').controller('appController', function(currentUser){
  appController = this;
  currentUser.fetch();

  appController.currentUser = currentUser;
});
