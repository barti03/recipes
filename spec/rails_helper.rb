ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
abort("The Rails environment is running in production mode!") if Rails.env.production?
require 'spec_helper'
require 'rspec/rails'
require 'paperclip/matchers'


Dir[Rails.root.join('spec/support/**/*.rb')].each { |f| require f }

ActiveRecord::Migration.maintain_test_schema!

RSpec.configure do |config|
  config.include JSON::SchemaMatchers
  config.include Paperclip::Shoulda::Matchers

  config.json_schemas[:recipes] = "spec/schemas/recipes.json"
  config.json_schemas[:recipe] = "spec/schemas/recipe.json"
  config.json_schemas[:ingredients] = "spec/schemas/ingredients.json"
  config.fixture_path = "#{::Rails.root}/spec/fixtures"
  config.use_transactional_fixtures = true
  config.infer_spec_type_from_file_location!
end
