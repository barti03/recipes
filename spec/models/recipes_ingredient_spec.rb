require 'rails_helper'

RSpec.describe RecipesIngredient, type: :model do
  it "has valid factory" do
    expect(create(:recipes_ingredient)).to be_valid
  end

  it do
    is_expected.to have_db_column(:amount)
      .of_type(:string).with_options(null: false)
  end
  it do
    is_expected.to have_db_column(:ingredient_id)
      .of_type(:integer).with_options(null: false)
  end
  it do
    is_expected.to have_db_column(:recipe_id)
      .of_type(:integer).with_options(null: false)
  end
  it do
    is_expected.to have_db_column(:created_at)
      .of_type(:datetime).with_options(null: false)
  end
  it do
    is_expected.to have_db_column(:updated_at)
      .of_type(:datetime).with_options(null: false)
  end

  it do
    is_expected.to have_db_index([:ingredient_id, :recipe_id]).unique(:true)
  end

  it { is_expected.to belong_to(:ingredient) }
  it { is_expected.to belong_to(:recipe).inverse_of(:recipes_ingredients) }

  it { is_expected.to validate_presence_of(:amount) }
  it { is_expected.to validate_presence_of(:ingredient) }
  it { is_expected.to validate_presence_of(:recipe) }

  # https://github.com/thoughtbot/shoulda-matchers/issues/535
  it { expect(create(:recipes_ingredient)).to validate_uniqueness_of(:ingredient).scoped_to(:recipe_id) }
end
