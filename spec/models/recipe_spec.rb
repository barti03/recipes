require 'rails_helper'

RSpec.describe Recipe, type: :model do
  it "has valid factory" do
    expect(create(:recipe)).to be_valid
  end

  it do
    is_expected.to have_db_column(:name)
      .of_type(:string).with_options(null: false)
  end
  it do
    is_expected.to have_db_column(:directions)
      .of_type(:text).with_options(null: false)
  end
  it do
    is_expected.to have_db_column(:created_by)
      .of_type(:string).with_options(null: false)
  end
  it do
    is_expected.to have_db_column(:created_at)
      .of_type(:datetime).with_options(null: false)
  end
  it do
    is_expected.to have_db_column(:updated_at)
      .of_type(:datetime).with_options(null: false)
  end
  it { is_expected.to have_db_column(:cover_file_name).of_type(:string) }
  it { is_expected.to have_db_column(:cover_content_type).of_type(:string) }
  it { is_expected.to have_db_column(:cover_file_size).of_type(:integer) }
  it { is_expected.to have_db_column(:cover_updated_at).of_type(:datetime) }

  it { is_expected.to have_many(:recipes_ingredients).inverse_of(:recipe) }
  it { is_expected.to have_many(:ingredients).through(:recipes_ingredients) }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:directions) }
  it { is_expected.to validate_presence_of(:recipes_ingredients) }
  it do
    is_expected.to validate_attachment_content_type(:cover)
    .allowing('image/png', 'image/jpeg')
    .rejecting('text/plain', 'text/xml')
  end

  it { is_expected.to accept_nested_attributes_for(:recipes_ingredients) }

  describe '.search' do
    it 'make proper elasticsearch query' do
      query = double('query');
      expect(Recipe).to receive_message_chain('__elasticsearch__.search')
        .with(
          {
            query: {
              multi_match: {
                query: query,
                fields: ['name'],
                fuzziness: 3
              }
            }
          }
        )
        Recipe.search(query)
    end
  end
end
