require 'rails_helper'

RSpec.describe Ingredient, type: :model do
  it "has valid factory" do
    expect(create(:ingredient)).to be_valid
  end

  it do
    is_expected.to have_db_column(:name)
      .of_type(:string).with_options(null: false)
  end
  it do
    is_expected.to have_db_column(:created_at)
      .of_type(:datetime).with_options(null: false)
  end
  it do
    is_expected.to have_db_column(:updated_at)
      .of_type(:datetime).with_options(null: false)
  end

  it { is_expected.to have_db_index(:name).unique(true) }

  it { is_expected.to have_many(:recipes_ingredients) }
  it { is_expected.to have_many(:recipes).through(:recipes_ingredients) }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_uniqueness_of(:name) }

  describe '.search' do
    it 'make proper elasticsearch query' do
      query = double('query');
      expect(Ingredient).to receive_message_chain('__elasticsearch__.search')
        .with(
          {
            query: {
              multi_match: {
                query: query,
                fields: ['name'],
                type: "phrase_prefix"
              }
            }
          }
        )
        Ingredient.search(query)
    end
  end
end
