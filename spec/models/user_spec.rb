require 'rails_helper'

class Koala::Facebook::APIError < Koala::KoalaError
  def initialize
  end
end

RSpec.describe User, type: :model do
  before do
    @api = double(:api, get_object:{"id" => '1234'})
    allow(Koala::Facebook::API).to receive(:new).and_return(@api)
  end

  describe 'if is initialized with valid access_token' do
    it 'looks for object "me"' do
      expect(@api).to receive(:get_object).with('me')
      User.new('4321')
    end

    it 'has id equal to value returned from facebook' do
      user = User.new('4321')
      expect(user.id).to eq('1234')
    end
  end

  describe 'if is initialized with invalid access_token' do
    before do
      allow(Koala::Facebook::AuthenticationError).to receive(:new)
      allow(Koala::Facebook::API).to receive(:new).and_raise(Koala::Facebook::AuthenticationError)
    end

    it 'has nil id' do
      user = User.new('123573')
      expect(user.id).to eq(nil)
    end
  end

  describe '#authenticated?' do
    before do
      @user = User.new('1234')
    end

    context 'when user id is falsey' do
      before do
        allow(@user).to receive(:id).and_return(nil)
      end

      it 'returns false' do
        expect(@user.authenticated?).to eq(false)
      end
    end

    context 'when user id is truthy' do
      before do
        allow(@user).to receive(:id).and_return('1234')
      end

      it 'returns false' do
        expect(@user.authenticated?).to eq(true)
      end
    end
  end

  describe '#owns?' do
    before do
      @user = User.new('1234')
      allow(@user).to receive(:id).and_return('1234')
    end

    context 'when recipe passed created_by is eq to user id' do
      before do
        @recipe = double(:recipe, created_by: @user.id)
      end

      it 'is truthy' do
        expect(@user.owns?(@recipe)).to be_truthy
      end
    end

    context 'when recipe passed created_by is not eq to user id' do
      before do
        @recipe = double(:recipe, created_by: '1254')
      end

      it 'is falsey' do
        expect(@user.owns?(@recipe)).to be_falsey
      end
    end
  end
end
