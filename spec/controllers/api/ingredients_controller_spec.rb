require 'rails_helper'

RSpec.describe API::IngredientsController, type: :controller do
  describe 'GET search' do
    before do
      @ingredients_found = [build_stubbed(:ingredient)]
      result = double('result')
      allow(result).to receive(:records).and_return(@ingredients_found)
      allow(Ingredient).to receive(:search).and_return(result)
    end

    it 'returns 200' do
      get :search, query: 'flour'
      expect(response).to have_http_status(200)
    end

    it 'assigns ingredients to ingredients found' do
      get :search, query: 'flour'
      expect(assigns(:ingredients)).to be(@ingredients_found)
    end

    it 'uses params query as parametr to Ingredient.search method' do
      expect(Ingredient).to receive(:search).with('flour')
      get :search, query: 'flour'
    end

    it 'returns json matching ingredients schema' do
      get :search, query: 'flour'
      expect(response.body).to match_json_schema(:ingredients)
    end

    it 'use IngredientSerializer to render response' do
      expect(IngredientSerializer).to receive(:new).and_call_original
      get :search, query: 'flour'
    end
  end
end
