require 'rails_helper'

RSpec.describe API::RecipesController, type: :controller do
  describe 'GET index' do
    before do
      @recipes = [build_stubbed(:recipe)]
      allow(Recipe).to receive(:order).with(created_at: :desc).and_return(@recipes)
    end

    it 'returns 200' do
      get :index
      expect(response).to have_http_status(200)
    end

    it 'assigns recipes to recipes all' do
      get :index
      expect(assigns(:recipes)).to eq(@recipes)
    end

    it 'use RecipeSerializer to render response' do
      expect(RecipesSerializer).to receive(:new).and_call_original
      get :index
    end

    it 'returns json matching recipes schema' do
      get :index
      expect(response.body).to match_json_schema(:recipes)
    end
  end

  describe 'GET show' do
    before do
      @recipe = build_stubbed(:recipe)
      allow(Recipe).to receive_message_chain(:includes, :find)
        .and_return(@recipe)
    end

    it 'returns 200' do
      get :show, id: 1
      expect(response).to have_http_status(200)
    end

    it 'assigns recipe' do
      get :show, id: 1
      expect(assigns(:recipe)).to eq(@recipe)
    end

    it 'includes ingredient to avoid n+1 queries' do
      expect(Recipe).to receive(:includes)
        .with(recipes_ingredients: [:ingredient])
      get :show, id: 1
    end

    it 'assigns recipe' do
      get :show, id: 1
      expect(assigns(:recipe)).to eq(@recipe)
    end

    it 'returns json matching recipe schema' do
      get :show, id: 1
      expect(response.body).to match_json_schema(:recipe)
    end
  end

  describe 'POST create' do
    before do
      @recipe = build_stubbed(:recipe)
      allow(Recipe).to receive(:new).and_return(@recipe)
    end

    context 'when user is not authenticated' do
      before do
        allow(User).to receive(:new).and_return(double(:user, id: 1, authenticated?: false))
      end

      it 'returns 401' do
        post :create
        expect(response).to have_http_status(401)
      end

      it 'renders nothing' do
        post :create
        expect(response.body).to be_blank
      end
    end

    context 'when user is authenticated' do
      before do
        allow(User).to receive(:new).and_return(double(:user, id: 1, authenticated?: true))
      end

      context 'when recipe is saved successfully' do
        before do
          allow(@recipe).to receive(:save).and_return(true)
          post :create
        end

        it 'returns 200' do
          expect(response).to have_http_status(200)
        end

        it 'renders json matching recipe schema' do
          expect(response.body).to match_json_schema(:recipe)
        end
      end
    end
  end

  describe 'PUT update' do
    before do
      @recipe = create(:recipe)
      @recipe_ingredient = create(:recipes_ingredient, {
        recipe_id: @recipe.id
      })
      @recipe_ingredient1 = create(:recipes_ingredient, {
        recipe_id: @recipe.id
      })
      @ingredient = create(:ingredient)
      @ingredient1 = create(:ingredient)
    end

    context 'when user is not authenticated' do
      before do
        allow(User).to receive(:new).and_return(double(:user, authenticated?: false))
      end

      it 'returns 401' do
        put :update, id: @recipe.id
        expect(response).to have_http_status(401)
      end

      it 'renders nothing' do
        put :update, id: @recipe.id
        expect(response.body).to be_blank
      end
    end

    context 'when user is authenticated' do
      context 'when user is not owner of recipe' do
        before do
          allow(User).to receive(:new).
            and_return(double(:user, authenticated?: true, owns?: false))
        end

        it 'returns 401' do
          put :update, id: @recipe.id
          expect(response).to have_http_status(401)
        end

        it 'renders nothing' do
          put :update, id: @recipe.id
          expect(response.body).to be_blank
        end
      end

      context 'when user is owner of recipe' do
        before do
          allow(User).to receive(:new).
            and_return(double(:user, authenticated?: true, owns?: true))
        end

        it 'assigns recipe based on id' do
          put :update, {
            id: @recipe.id,
            recipes_ingredients_attributes: {
              '0' => {
                id: @recipe_ingredient.id,
                ingredient_id: @ingredient.id,
                amount: '2g'
              }
            }
          }

          expect(assigns(:recipe).id).to eq(@recipe.id)
        end

        it 'updates recipe' do
          allow(Recipe).to receive_message_chain('includes.find')
            .and_return(@recipe)

          expect(@recipe).to receive(:update_attributes)
          put :update, {
            id: @recipe.id,
            recipes_ingredients_attributes: {
              '0' => {
                id: @recipe_ingredient.id,
                ingredient_id: @ingredient.id,
                amount: '2g'
              }
            }
          }
        end

        context 'when update is successful' do
          before do
            allow(Recipe).to receive_message_chain('includes.find')
              .and_return(@recipe)
            allow(@recipe).to receive(:update_attributes).and_return(true)
          end

          it 'destroys all recipes ingredients related to recipe not mentioned in params' do
            expect do
            put :update, {
              id: @recipe.id,
              recipes_ingredients_attributes: {
                '0' => {
                  id: @recipe_ingredient.id,
                  ingredient_id: @ingredient.id,
                  amount: '2g'
                },
                '1' => {
                  id: @recipe_ingredient1.id,
                  ingredient_id: @ingredient1.id,
                  amount: '2ml'
                }
              }
            }
            end.to change { @recipe.recipes_ingredients.count }.from(3).to(2)
          end

          it 'returns 200' do
            put :update, {
              id: @recipe.id,
              recipes_ingredients_attributes: {
                '0' => {
                  id: @recipe_ingredient.id,
                  ingredient_id: @ingredient.id,
                  amount: '2g'
                },
                '1' => {
                  id: @recipe_ingredient1.id,
                  ingredient_id: @ingredient1.id,
                  amount: '2ml'
                }
              }
            }

            expect(response).to have_http_status(200)
          end

          it 'renders json matching recipe schema' do
            put :update, {
              id: @recipe.id,
              recipes_ingredients_attributes: {
                '0' => {
                  id: @recipe_ingredient.id,
                  ingredient_id: @ingredient.id,
                  amount: '2g'
                },
                '1' => {
                  id: @recipe_ingredient1.id,
                  ingredient_id: @ingredient1.id,
                  amount: '2ml'
                }
              }
            }

            expect(response.body).to match_json_schema(:recipe)
          end
        end


        context 'when update is not successfull' do
          before do
            allow(Recipe).to receive_message_chain('includes.find')
              .and_return(@recipe)
            allow(@recipe).to receive(:update_attributes).and_return(false)
            put :update, {
              id: @recipe.id,
              recipes_ingredients_attributes: {
                '0' => {
                  id: @recipe_ingredient.id,
                  ingredient_id: @ingredient.id,
                  amount: '2g'
                },
                '1' => {
                  id: @recipe_ingredient1.id,
                  ingredient_id: @ingredient1.id,
                  amount: '2ml'
                }
              }
            }
          end

          it 'retuns 422' do
            expect(response).to have_http_status(422)
          end

          it 'renders nothing' do
            expect(response.body).to be_blank
          end
        end
      end
    end
  end

  describe 'GET search' do
    before do
      @recipes = create_list(:recipe, 2)
      @recipes_named_similarly = create_list(:recipe, 3)
      @common_ingredients = create_list(:ingredient, 2)
      create(
        :recipes_ingredient,
        ingredient: @common_ingredients.first,
        recipe: @recipes.first
      )
      create(
        :recipes_ingredient,
        ingredient: @common_ingredients.first,
        recipe: @recipes_named_similarly.first
      )
      create(
        :recipes_ingredient,
        ingredient: @common_ingredients.first,
        recipe: @recipes_named_similarly.second
      )
      create(
        :recipes_ingredient,
        ingredient: @common_ingredients.second,
        recipe: @recipes.first
      )
      create(
        :recipes_ingredient,
        ingredient: @common_ingredients.second,
        recipe: @recipes_named_similarly.first
      )
      @recipes_with_the_same_ingredients = [
        @recipes.first,
        @recipes_named_similarly.first
      ]
      @recipes_with_the_same_ingredient_and_named_similarly = [
        @recipes_named_similarly.first
      ]

      result = double('result')
      allow(result)
        .to receive(:records)
        .and_return(Recipe.where(id: @recipes_named_similarly.map(&:id)))
      allow(Recipe).to receive(:search).and_return(result)
    end

    context 'with query param' do
      it 'returns 200' do
        get :search, query: 'pizza'
        expect(response).to have_http_status(200)
      end

      it 'assigns recipes to recipes matching query param value' do
        get :search, query: 'pizza'
        expect(assigns(:recipes)).to match_array(@recipes_named_similarly)
      end

      it 'uses params query as parametr to Recipe.search method' do
        expect(Recipe).to receive(:search).with('pizza')
        get :search, query: 'pizza'
      end

      it 'returns json matching recipes schema' do
        get :search, query: 'pizza'
        expect(response.body).to match_json_schema(:recipes)
      end

      it 'use RecipeSerializer to render response' do
        expect(RecipesSerializer)
          .to receive(:new).exactly(3).times.and_call_original
        get :search, query: 'pizza'
      end
    end

    context 'with ingredients param' do
      it 'assigns recipes to recipes with name matching query param value' do
        get :search, ingredients: @common_ingredients.map(&:id).join(',')
        expect(assigns(:recipes))
          .to match_array(@recipes_with_the_same_ingredients)
      end
    end

    context 'with ingredients param' do
      it 'returns each recipe that contains all ingredients' do
        get :search, ingredients: @common_ingredients.map(&:id).join(',')
        expect(assigns(:recipes))
          .to match_array(@recipes_with_the_same_ingredients)
      end
    end

    context 'with query and ingredients params' do
      it 'assigns recipes to recipes which name match query and contain' \
      'all ingredients' do
        get(
          :search,
          ingredients: @common_ingredients.map(&:id).join(','), query: 'pizza'
        )
        expect(assigns(:recipes))
          .to match_array(@recipes_with_the_same_ingredient_and_named_similarly)
      end
    end
  end

  describe 'DELETE destroy' do
    before do
      @recipe = create(:recipe)
    end

    context 'when user is not authenticated' do
      before do
        allow(User).to receive(:new).and_return(double(:user, authenticated?: false))
      end

      it 'returns 401' do
        delete :destroy, id: @recipe.id
        expect(response).to have_http_status(401)
      end

      it 'renders nothing' do
        delete :destroy, id: @recipe.id
        expect(response.body).to be_blank
      end
    end

    context 'when user is authenticated' do
      context 'when user is not owner of recipe' do
        before do
          allow(User).to receive(:new).
            and_return(double(:user, authenticated?: true, owns?: false))
        end

        it 'returns 401' do
          delete :destroy, id: @recipe.id
          expect(response).to have_http_status(401)
        end

        it 'renders nothing' do
          delete :destroy, id: @recipe.id
          expect(response.body).to be_blank
        end
      end

      context 'when user is owner of recipe' do
        before do
          allow(User).to receive(:new).
            and_return(double(:user, authenticated?: true, owns?: true))
        end

        it 'destroys recipe' do
          expect { delete :destroy, id: @recipe.id }.to change { Recipe.count }.by(-1)
        end
      end
    end
  end
end
