FactoryGirl.define do
  factory :recipes_ingredient do
    amount 500
    recipe
    ingredient
  end
end
