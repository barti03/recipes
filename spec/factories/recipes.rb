FactoryGirl.define do
  factory :recipe do
    name "Apple Pie"
    directions "Mix flour and butter, then..."
    created_by "12345"

    after(:build) do |recipe|
      recipe.recipes_ingredients << build(:recipes_ingredient, recipe: recipe)
    end

    after(:stub) do |recipe|
      recipe.recipes_ingredients << build(:recipes_ingredient, recipe: recipe)
    end
  end
end
