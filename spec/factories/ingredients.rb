FactoryGirl.define do
  factory :ingredient do
    sequence(:name) { |n| "Flour (type ##{n})" }
  end
end
