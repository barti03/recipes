class API::IngredientsController < ApplicationController
  include ActionController::Serialization

  def search
    if params[:query]
      @ingredients = Ingredient.search(params[:query]).records
    else
      @ingredients = []
    end

    render json: @ingredients, each_serializer: IngredientSerializer
  end
end
