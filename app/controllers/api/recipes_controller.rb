class API::RecipesController < ApplicationController
  include ActionController::Serialization
  before_action :set_recipe, only: :show
  before_action :authenticate, only: :create
  before_action :set_recipe_and_authenticate, only: [:update, :destroy]

  def index
    @recipes = Recipe.order(created_at: :desc)

    render json: @recipes, each_serializer: RecipesSerializer
  end

  def show
    render json: @recipe
  end

  def create
    @recipe = Recipe.new(recipe_params)
    @recipe.created_by = @user.id
    if @recipe.save
      render json: @recipe
    else
      render nothing: true, status: :unprocessable_entity
    end
  end

  def update
    ingredients_ids_to_remove = ingredients_ids_to_remove_from_params

    if @recipe.update_attributes(recipe_params)
      RecipesIngredient.where(id: ingredients_ids_to_remove).destroy_all
      render json: @recipe
    else
      render nothing: true, status: :unprocessable_entity
    end
  end

  def destroy
    @recipe.destroy

    render json: @recipe
  end

  def search
    if params[:query]
      @recipes = Recipe.search(params[:query]).records
    end

    if params[:ingredients]
      ingredients_ids= params[:ingredients].split(',').uniq
      @recipes = (@recipes || Recipe).joins(:recipes_ingredients)
        .where('recipes_ingredients.ingredient_id': ingredients_ids)
        .group(:id)
        .having('count(*) = ?', ingredients_ids.length)
    end
    @recipes ||= []

    render json: @recipes, each_serializer: RecipesSerializer
  end

  private
    def recipe_params
      params.permit(:name, :directions, :cover, :id, recipes_ingredients_attributes: [:ingredient_id, :amount, :id])
    end

    def ingredients_ids_to_remove_from_params
      current_ingredients = @recipe.recipes_ingredients.ids
      future_ingredients = recipe_params['recipes_ingredients_attributes']
        .map {|k, v| v['id'] && v['id'].to_i}
      current_ingredients - future_ingredients
    end

    def set_recipe
      @recipe = Recipe.includes(recipes_ingredients: [:ingredient]).find(params[:id])
    end

    def authenticate
      @user = User.new(params['access_token']);
      return if @user.authenticated? && (!@recipe || @user.owns?(@recipe))
      render nothing: true, status: :unauthorized and return
    end

    def set_recipe_and_authenticate
      set_recipe
      authenticate
    end
end
