class ApplicationController < ActionController::API
  include ActionController::Serialization

  def facebook_cookies
    @facebook_cookies ||= Koala::Facebook::OAuth.new.get_user_info_from_cookie(cookies)
  end

  def default_serializer_options
    {root: false}
  end
end
