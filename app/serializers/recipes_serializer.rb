class RecipesSerializer < ActiveModel::Serializer
  attributes :id, :name, :cover_thumb, :created_by

  def cover_thumb
    object.cover.file? ? object.cover.url(:thumb) : nil
  end
end
