class RecipeSerializer < ActiveModel::Serializer
  attributes :id, :name, :directions
  has_many :recipes_ingredients
end
