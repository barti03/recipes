class RecipeSerializer < ActiveModel::Serializer
  attributes :id, :name, :directions, :cover_wide
  has_many :recipes_ingredients

  def cover_wide
    object.cover.file? ? object.cover.url(:wide) : nil
  end
end
