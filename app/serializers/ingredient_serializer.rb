class IngredientSerializer < ActiveModel::Serializer
  attributes :name, :ingredient_id

  def ingredient_id
    object.id
  end
end
