class RecipesIngredientSerializer < ActiveModel::Serializer
  attributes :amount, :id
  has_one :ingredient
end
