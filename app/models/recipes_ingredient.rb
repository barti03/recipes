class RecipesIngredient < ActiveRecord::Base
  belongs_to :ingredient
  belongs_to :recipe, inverse_of: :recipes_ingredients

  validates :amount, presence: true
  validates :ingredient, presence: true, uniqueness: { scope: :recipe_id }
  validates :recipe, presence: true
end
