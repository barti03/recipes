class Ingredient < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  has_many :recipes_ingredients
  has_many :recipes, through: :recipes_ingredients

  validates :name, presence: true, uniqueness: true

  def self.search(query)
    __elasticsearch__.search(
      {
        query: {
          multi_match: {
            query: query,
            fields: ['name'],
            type: "phrase_prefix"
          }
        }
      }
    )
  end
end
