class Recipe < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  has_many :recipes_ingredients, inverse_of: :recipe
  has_many :ingredients, through: :recipes_ingredients
  has_attached_file :cover, styles: { wide: '1200x300#', thumb: '246x176#' }

  validates :name, presence: true
  validates :directions, presence: true
  validates :recipes_ingredients, presence: true
  validates_attachment_content_type :cover, content_type: /\Aimage\/.*\Z/

  accepts_nested_attributes_for :recipes_ingredients

  def self.search(query)
    __elasticsearch__.search(
      {
        query: {
          multi_match: {
            query: query,
            fields: ['name'],
            fuzziness: 3
          }
        }
      }
    )
  end
end
