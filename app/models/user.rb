class User
  def initialize(access_token)
    begin
      @api = Koala::Facebook::API.new(access_token)
      @instance = @api.get_object('me')
    rescue Koala::Facebook::AuthenticationError
      @api = nil
      @instance = nil
    end
  end

  def id
    @instance['id'] if @instance
  end

  def authenticated?
    !!id
  end

  def owns?(recipe)
    recipe.created_by == id
  end
end
